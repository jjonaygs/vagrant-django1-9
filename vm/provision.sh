# Run the provisioning script in noninteractive mode
export DEBIAN_FRONTEND=noninteractive

# Edit the following to change the version of PostgreSQL that is installed
PG_VERSION=9.3

# Update the package system first
sudo apt-get -y update

# Install python3 packages
sudo apt-get install -y python-software-properties
sudo add-apt-repository -y ppa:fkrull/deadsnakes
sudo apt-get -y update

sudo apt-get install python-psycopg2
sudo apt-get install -y build-essential checkinstall
sudo apt-get install -y libreadline-gplv2-dev
sudo apt-get install -y libncursesw5-dev
sudo apt-get install -y libssl-dev
sudo apt-get install -y libsqlite3-dev
sudo apt-get install -y tk-dev
sudo apt-get install -y libgdbm-dev
sudo apt-get install -y libc6-dev
sudo apt-get install -y libbz2-dev
sudo apt-get install -y libjpeg-dev
sudo apt-get install -y libtiff5-dev
sudo apt-get install -y zlib1g-dev
sudo apt-get install -y python3.4
sudo apt-get install -y python3-dev


sudo apt-get install -y curl
sudo apt-get install -y zsh
sudo apt-get install -y git



# Install dependecies
sudo apt-get build-dep -y ipython

# First run configuration
if [ ! -f /home/vagrant/.first_run ]; then
    echo ">>> First run <<<"
    touch /home/vagrant/.first_run

    # Set and configure zsh as the default shell
    git clone git://github.com/robbyrussell/oh-my-zsh.git /home/vagrant/.oh-my-zsh
    cp /home/vagrant/.oh-my-zsh/templates/zshrc.zsh-template /home/vagrant/.zshrc
    chsh -s /usr/bin/zsh vagrant

    # Add some custom configuration to zsh
    echo "# Custom Vagrant configuration" >> /home/vagrant/.zshrc
    echo "bindkey '[D' backward-word" >> /home/vagrant/.zshrc
    echo "bindkey '[C' forward-word" >> /home/vagrant/.zshrc
    echo "cd /vagrant-django1-9" >> /home/vagrant/.zshrc

    # Enable profile switching for iterm2
    echo "echo -e '\033]50;SetProfile=Vagrant\a'" >> /home/vagrant/.zlogin
    echo "echo -e '\033]50;SetProfile=Default\a'" >> /home/vagrant/.zlogout

    # Install pip
    curl -s https://bootstrap.pypa.io/get-pip.py | sudo python3.4

    # Make vagrant user own /home
    chown -R vagrant /home
fi

# Install python requirements
sudo pip install --upgrade setuptools
sudo pip install -r /vagrant/requirements.txt
